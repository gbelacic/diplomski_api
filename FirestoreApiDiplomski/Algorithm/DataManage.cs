﻿using FirestoreApiDiplomski.DataAccess;
using FirestoreApiDiplomski.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace FirestoreApiDiplomski.Algorithm
{
    public class DataManage
    {
        FirestoreDataAccess _firestoreDb;
        public DataManage()
        {
            _firestoreDb = new FirestoreDataAccess();
        }

        public async Task<bool> CheckAndDeleteUnnecessaryData()
        {
            try
            {
                var allCells = await _firestoreDb.GetAllNetworkCells();

                var suspiciousCells = await _firestoreDb.GetNetworkCells("SuspiciousCells");
                var deletedCells = await _firestoreDb.GetNetworkCells("DeletedCells");

                var date = DateTime.Now.AddDays(-2).Date; //3 months ago

                var oldData = allCells.Where(x => x.Location != null &&
                                                !string.IsNullOrEmpty(x.Location.DateTime) &&
                                                DateTime.ParseExact(x.Location.DateTime, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).Date < date);

                var newData = allCells.Where(x => x.Location != null &&
                                                !string.IsNullOrEmpty(x.Location.DateTime) &&
                                                DateTime.ParseExact(x.Location.DateTime, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).Date > date);

                foreach (var oldCell in oldData)
                {
                    var findNewCellNearby = newData.FirstOrDefault(x => x.Location != null &&
                                            GetDistance(x.Location.Longitude, x.Location.Latitude,
                                                        oldCell.Location.Longitude, oldCell.Location.Latitude) < 20);

                    if (findNewCellNearby != null)
                    {
                        if (findNewCellNearby.Neighbours[0].CellId != oldCell.Neighbours[0].CellId)
                        {
                            if (oldCell.Neighbours.Where(x => x.CellId != findNewCellNearby.Neighbours[0].CellId).Any())
                            {
                                if (suspiciousCells.Contains(oldCell)) //delete from suspicious and insert to deleted
                                {
                                    await _firestoreDb.DeleteFromSuspicious(oldCell.DocumentId);

                                    await _firestoreDb.AddToDeleted(oldCell);

                                }
                                else
                                { //add to suspicious
                                    await _firestoreDb.AddToSuspicious(oldCell);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }



        public double GetDistance(double longitude, double latitude, double otherLongitude, double otherLatitude)
        {
            var d1 = latitude * (Math.PI / 180.0);
            var num1 = longitude * (Math.PI / 180.0);
            var d2 = otherLatitude * (Math.PI / 180.0);
            var num2 = otherLongitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }
    }
}
