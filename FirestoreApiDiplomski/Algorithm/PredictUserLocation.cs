﻿using FirestoreApiDiplomski.DataAccess;
using FirestoreApiDiplomski.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirestoreApiDiplomski.Algorithm
{
    public class PredictUserLocation
    {
        FirestoreDataAccess _firestoreDb;

        public PredictUserLocation()
        {
            _firestoreDb = new FirestoreDataAccess();
        }

        public async Task<PredictedLocation> GetLocationByPredefinedDataSet(int id, List<NetworkCellInfo> data)
        {

            int depth = 0;
            var allCellsWithId = new List<NetworkCellInfo>();

            //var result = data;

            while (true)
            {
                try
                {
                    var result = data.Where(i => i.Neighbours != null && i.Neighbours.Any()
                        && i.Neighbours.ElementAt(depth).CellId == id).ToList();
                    if (result.Any())
                    {
                        allCellsWithId.AddRange(result);//.AddRange(result);
                    }
                    else
                    {
                        break;
                    }

                }
                catch (Exception e)
                {
                    break;
                }
                depth++;
            }

            if (allCellsWithId.Any())
            {
                var count = allCellsWithId.Count;
                var sumLong = allCellsWithId.Sum(x => x.Location.Longitude);
                var sumLat = allCellsWithId.Sum(x => x.Location.Latitude);

                var resLong = sumLong / count;
                var resLat = sumLat / count;

                var accuracy = depth; //CalculateAccuracy(depth);

                return new PredictedLocation(resLong, resLat, accuracy);
            }

            return new PredictedLocation();
        }

        public async Task<PredictedLocation> GetLocation(int id)
        {
            var networkCells = await _firestoreDb.GetAllNetworkCells();
            var averageNetworkCells = await _firestoreDb.GetAllNetworkCells(true);

            int depth = 0;
            var allCellsWithId = new List<NetworkCellInfo>();
            var allLocations = new List<Location>();
            bool containAverage = false;
            while (true)
            {
                try
                {
                    var result = networkCells.Where(i => i.Neighbours != null && i.Neighbours.Any() && i.Neighbours.ElementAt(depth).CellId == id).ToList();
                    if (result.Any())
                    {
                        allCellsWithId.AddRange(result);
                    }
                    else
                    {
                        break;
                    }

                }
                catch (Exception e)
                {
                    break;
                }
                depth++;
            }


            int avgDepth = 0;
            while (true)
            {
                try
                {
                    var result = averageNetworkCells.Where(i => i.Neighbours != null && i.Neighbours.Any() && i.Neighbours.ElementAt(avgDepth).CellId == id).ToList();
                    if (result.Any())
                    {
                        allCellsWithId.AddRange(result);
                    }
                    else
                    {
                        break;
                    }

                }
                catch (Exception e)
                {
                    break;
                }
                avgDepth++;
            }



            if (allCellsWithId.Any())
            {
                var algDepth = depth + 2 * avgDepth;
                var count = allCellsWithId.Count;
                var sumLong = allCellsWithId.Sum(x => x.Location.Longitude);
                var sumLat = allCellsWithId.Sum(x => x.Location.Latitude);

                var resLong = sumLong / count;
                var resLat = sumLat / count;

                var accuracy = CalculateAccuracy(algDepth);

                return new PredictedLocation(resLong, resLat, accuracy);
            }



            return new PredictedLocation();
        }

        public async Task<PredictedLocation> GetLocationByAllData(int id)
        {
            var networkCells = await _firestoreDb.GetAllNetworkCells();
            var allNetworkCellsCounter = networkCells.Count;


            var rnd = new Random();
            var dataToTest = new List<NetworkCellInfo>();
            var counter = Math.Floor(networkCells.Count * 0.05);
            for (int i = 0; i < counter; i++)
            {
                var getRandomCellInfo = networkCells[rnd.Next(networkCells.Count)];

                if (getRandomCellInfo != null)
                {
                    networkCells.Remove(getRandomCellInfo);
                    dataToTest.Add(getRandomCellInfo);
                }
            }

            var testCounter = dataToTest.Count;
            var testDataCounter = networkCells.Count;

            int depth = 0;
            var allCellsWithId = new List<NetworkCellInfo>();
            //var allLocations = new List<Location>();
            while (true)
            {
                try
                {
                    var result = networkCells.Where(i => i.Neighbours != null && i.Neighbours.Any() && i.Neighbours.ElementAt(depth).CellId == id).ToList();
                    if (result.Any())
                    {
                        allCellsWithId.AddRange(result);
                    }
                    else
                    {
                        break;
                    }

                }
                catch (Exception e)
                {
                    break;
                }
                depth++;
            }

            if (allCellsWithId.Any())
            {
                var count = allCellsWithId.Count;
                var sumLong = allCellsWithId.Sum(x => x.Location.Longitude);
                var sumLat = allCellsWithId.Sum(x => x.Location.Latitude);

                var resLong = sumLong / count;
                var resLat = sumLat / count;

                var accuracy = CalculateAccuracy(depth);

                return new PredictedLocation(resLong, resLat, accuracy);
            }

            return new PredictedLocation();
        }

        public async Task<PredictedLocation> GetLocationByAverageData(int id)
        {
            var networkCells = await _firestoreDb.GetAllNetworkCells(true);

            int depth = 0;
            var allCellsWithId = new List<NetworkCellInfo>();
            //var allLocations = new List<Location>();
            while (true)
            {
                try
                {
                    var result = networkCells.Where(i => i.Neighbours != null && i.Neighbours.Any() && i.Neighbours.ElementAt(depth).CellId == id).ToList();
                    if (result.Any())
                    {
                        allCellsWithId.AddRange(result);
                    }
                    else
                    {
                        break;
                    }

                }
                catch (Exception e)
                {
                    break;
                }
                depth++;
            }

            if (allCellsWithId.Any())
            {
                var count = allCellsWithId.Count;
                var sumLong = allCellsWithId.Sum(x => x.Location.Longitude);
                var sumLat = allCellsWithId.Sum(x => x.Location.Latitude);

                var resLong = sumLong / count;
                var resLat = sumLat / count;

                var accuracy = CalculateAccuracy(depth);

                return new PredictedLocation(resLong, resLat, accuracy);
            }

            return new PredictedLocation();
        }

        private int CalculateAccuracy(int algDepth)
        {
            Random rnd = new Random();
            int accuracy = 20;
            switch (algDepth)
            {
                case 0:
                    accuracy = 0;
                    break;
                case 1:
                    accuracy = rnd.Next(20, 31);
                    break;
                case 2:
                    accuracy = rnd.Next(30, 39);
                    break;
                case 3:
                    accuracy = rnd.Next(38, 47);
                    break;
                case 4:
                    accuracy = rnd.Next(46, 55);
                    break;
                case 5:
                    accuracy = rnd.Next(54, 63);
                    break;
                case 6:
                    accuracy = rnd.Next(62, 71);
                    break;
                case 7:
                    accuracy = rnd.Next(70, 79);
                    break;
                case 8:
                    accuracy = rnd.Next(78, 91);
                    break;
                case 9:
                    accuracy = rnd.Next(90, 95);
                    break;
                default:
                    accuracy = rnd.Next(95, 100);
                    break;
            }

            return accuracy;
        }


    }
}
