﻿using FirestoreApiDiplomski.DataAccess;
using FirestoreApiDiplomski.Models;
using Google.Type;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace FirestoreApiDiplomski.Algorithm
{
    public class ResultsTest
    {
        FirestoreDataAccess _firestoreDb;
        PredictUserLocation _algorithm;
        public ResultsTest()
        {
            _firestoreDb = new FirestoreDataAccess();

            _algorithm = new PredictUserLocation();
        }

        public async Task<Tuple<List<NetworkCellInfo>, List<NetworkCellInfo>>> PrepareTestData(int data, int avg)
        {
            //initialize
            var isAvg = avg == 1 ? true : false;
            var networkCells = new List<NetworkCellInfo>();

            if (data == 0 || data == 2) {
                networkCells = await _firestoreDb.GetAllNetworkCells(isAvg);
            }
            if (data == 1) {
                networkCells = await _firestoreDb.GetAllNetworkCellsForTesting(isAvg);
            }
            if (data == 2) {
                var testingData = await _firestoreDb.GetAllNetworkCellsForTesting(isAvg);
                networkCells.AddRange(testingData);
            }
            
            //var networkCells = await _firestoreDb.GetAllNetworkCellsForTesting();

            var sumLat = networkCells.Sum(x => x.Location?.Latitude);
            var sumLong = networkCells.Sum(x => x.Location?.Longitude);

            var avgLat = sumLat / networkCells.Count;
            var avgLong = sumLong / networkCells.Count;

            var rnd = new Random();
            var dataToTest = new List<NetworkCellInfo>();
            var counter = Math.Floor(networkCells.Count * 0.05);
            for (int i = 0; i < counter; i++)
            {
                var getRandomCellInfo = networkCells[rnd.Next(networkCells.Count)];

                if (getRandomCellInfo != null)
                {
                    networkCells.Remove(getRandomCellInfo);
                    dataToTest.Add(getRandomCellInfo);
                }
            }

            var testData = new Tuple<List<NetworkCellInfo>, List<NetworkCellInfo>>(networkCells, dataToTest);

            return testData;
        }

        public async Task<int> Test(int data, int avg)
        {
            var prepareData = await PrepareTestData(data, avg);

            var queryData = prepareData.Item2;
            var testData = prepareData.Item1;

            var results = new List<KeyValuePair<NetworkCellInfo, PredictedLocation>>();
            foreach (var query in queryData)
            {
                var result = await _algorithm.GetLocationByPredefinedDataSet(query.CellId, testData);

                if (result != null)
                    results.Add(new KeyValuePair<NetworkCellInfo, PredictedLocation>(query, result));
            }

            var testOvo = await CalculateDistance(results);
            return 0;
        }

        public async Task<int> CalculateDistance(List<KeyValuePair<NetworkCellInfo, PredictedLocation>> results)
        {
            var distances = new List<Tuple<double, int>>();
            foreach (var res in results)
            {

                var dist = GetDistance(res.Key.Location.Longitude, res.Key.Location.Latitude, res.Value.Longitude, res.Value.Latitude);

                //if(dist < 40)
                distances.Add(new Tuple<double, int>(dist, res.Value.Accuracy));
            }
            distances = distances.Where(x => x.Item1 < 390).ToList();

            var grouped = distances.GroupBy(i => i.Item2);

            foreach (var group in grouped)
            {
                var values = new List<double>();
                var depth = group.Key;
                var min = group.ToList().Min(x => x.Item1);
                var max = group.ToList().Max(x => x.Item1);

                var sum = group.ToList().Sum(x => x.Item1);
                var avg = sum / group.ToList().Count;

                foreach (var x in group.ToList()) {
                    values.Add(x.Item1);
                }
                IEnumerable<double> hh = new List<double>(values);

                var stDev = CalculateStdDev(hh);
             }
            return 0;
        }


        public double GetDistance(double longitude, double latitude, double otherLongitude, double otherLatitude)
        {
            var d1 = latitude * (Math.PI / 180.0);
            var num1 = longitude * (Math.PI / 180.0);
            var d2 = otherLatitude * (Math.PI / 180.0);
            var num2 = otherLongitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }

        private double CalculateStdDev(IEnumerable<double> values)
        {
            double ret = 0;
            if (values.Count() > 0)
            {
                //Compute the Average      
                double avg = values.Average();
                //Perform the Sum of (value-avg)_2_2      
                double sum = values.Sum(d => Math.Pow(d - avg, 2));
                //Put it all together      
                ret = Math.Sqrt((sum) / (values.Count() - 1));
            }
            return ret;
        }


    }
}
