﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirestoreApiDiplomski.Models
{
    public class PredictedLocation
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public int Accuracy { get; set; }


        public PredictedLocation() { }

        public PredictedLocation(double lon, double lat, int acc)
        {
            Longitude = lon;
            Latitude = lat;
            Accuracy = acc;
        }
    }
}
