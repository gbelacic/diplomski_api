﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirestoreApiDiplomski.Models
{
    [FirestoreData]
    public class NetworkCellInfo
    {
        public string DocumentId { get; set; } 
        public int CellId { get; set; }
        public string DeviceIMEI { get; set; }
        public Location Location { get; set; }

        public List<Neighbour> Neighbours { get; set; }

        public NetworkCellInfo()
        {
            Neighbours = new List<Neighbour>();
        }
    }
}
