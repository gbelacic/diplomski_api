﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirestoreApiDiplomski.Models
{
    [FirestoreData]
    public class Neighbour
    {
        public int AsuLevel { get; set; }
        public int CellId { get; set; }
        public int Dbm { get; set; }
        public int Level { get; set; }
        public int Rsrp { get; set; }
        public int Rsrq { get; set; }
        public int Rssnr { get; set; }
        public int Lac { get; set; }
        public int Pci { get; set; }
        public string Technology { get; set; }
        public int TimingAdvance { get; set; }

        public virtual NetworkCellInfo NetworkCellInfo {get; set;}

    }
}
