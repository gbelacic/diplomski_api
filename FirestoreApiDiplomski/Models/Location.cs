﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirestoreApiDiplomski.Models
{
    [FirestoreData]
    public class Location
    {
        public float Altitude { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string DateTime { get; set; }
    }
}
