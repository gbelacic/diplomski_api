﻿using FirestoreApiDiplomski.Models;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirestoreApiDiplomski.DataAccess
{
    public class FirestoreDataAccess
    {
        string projectId;
        FirestoreDb fireStoreDb;

        public FirestoreDataAccess()
        {
            string filepath = "PrivateKeys\\diplomskiprojekt-cf8f2-cbf5dced6eda.json";
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", filepath);
            projectId = "diplomskiprojekt-cf8f2";
            fireStoreDb = FirestoreDb.Create(projectId);
        }

        public async Task<List<NetworkCellInfo>> GetAllNetworkCells(bool isAverageValues = false)
        {
            var networkCells = new List<NetworkCellInfo>(); //create empty list of network cells

            var collectionName = isAverageValues ? "AverageNetworkCellInfo" : "NetworkCellInfo"; //choose which collection to use

            try
            {
                Query allCellsQuery = fireStoreDb.Collection(collectionName); // create query to get all cells info
                QuerySnapshot allCellsQuerySnapshot = await allCellsQuery.GetSnapshotAsync(); //get data

                //get all to readable data
                foreach (DocumentSnapshot documentSnapshot in allCellsQuerySnapshot.Documents)
                {
                    if (documentSnapshot.Exists)
                    {
                        var data = documentSnapshot.ToDictionary();
                        string json = JsonConvert.SerializeObject(data);

                        NetworkCellInfo cell = JsonConvert.DeserializeObject<NetworkCellInfo>(json);

                        networkCells.Add(cell);

                        cell.DocumentId = documentSnapshot.Id;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Err msg: " + e.Message);
            }

            return networkCells;
        }

        public async Task<List<NetworkCellInfo>> GetAllNetworkCellsForTesting(bool isAverageValues = false)
        {
            var networkCells = new List<NetworkCellInfo>(); //create empty list of network cells

            var collectionName = isAverageValues ? "TestResultsAverage" : "TestResults"; //choose  collection

            try
            {
                Query allCellsQuery = fireStoreDb.Collection(collectionName); // create query to get all info
                QuerySnapshot allCellsQuerySnapshot = await allCellsQuery.GetSnapshotAsync(); //get data

                //get all to readable data
                foreach (DocumentSnapshot documentSnapshot in allCellsQuerySnapshot.Documents)
                {
                    if (documentSnapshot.Exists)
                    {
                        var data = documentSnapshot.ToDictionary();
                        string json = JsonConvert.SerializeObject(data);
                        NetworkCellInfo cell = JsonConvert.DeserializeObject<NetworkCellInfo>(json);
                        networkCells.Add(cell);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Err msg: " + e.Message);
            }

            return networkCells;
        }

        public async Task<List<NetworkCellInfo>> GetNetworkCells(string collectionName)
        {
            var networkCells = new List<NetworkCellInfo>(); //create empty list of network cells


            try
            {
                Query allCellsQuery = fireStoreDb.Collection(collectionName); // create query to get all cells info
                QuerySnapshot allCellsQuerySnapshot = await allCellsQuery.GetSnapshotAsync(); //get data

                //get all to readable data
                foreach (DocumentSnapshot documentSnapshot in allCellsQuerySnapshot.Documents)
                {
                    if (documentSnapshot.Exists)
                    {
                        var data = documentSnapshot.ToDictionary();
                        string json = JsonConvert.SerializeObject(data);

                        NetworkCellInfo cell = JsonConvert.DeserializeObject<NetworkCellInfo>(json);

                        networkCells.Add(cell);

                        cell.DocumentId = documentSnapshot.Id;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Err msg: " + e.Message);
            }

            return networkCells;
        }

        public async Task<List<NetworkCellInfo>> GetNetworkCellsById(int id, bool isAverageValues = false)
        {
            var networkCells = new List<NetworkCellInfo>(); //create empty list of network cells

            var collectionName = isAverageValues ? "AverageNetworkCellInfo" : "NetworkCellInfo"; //choose which collection to use

            try
            {
                Query allCellsQuery = fireStoreDb.Collection(collectionName).WhereEqualTo("cellId", id); // create query to get celly by id
                QuerySnapshot allCellsQuerySnapshot = await allCellsQuery.GetSnapshotAsync(); //get data

                //get all to readable data
                foreach (DocumentSnapshot documentSnapshot in allCellsQuerySnapshot.Documents)
                {
                    if (documentSnapshot.Exists)
                    {
                        var data = documentSnapshot.ToDictionary();
                        string json = JsonConvert.SerializeObject(data);

                        NetworkCellInfo cell = JsonConvert.DeserializeObject<NetworkCellInfo>(json);

                        networkCells.Add(cell);
                    }
                }
            }
            catch
            {
                //TODO throw;
            }

            return networkCells;
        }

        public async Task DeleteFromSuspicious(string documentId) {

            DocumentReference cityRef = fireStoreDb.Collection("SuspiciousCells").Document(documentId);
            await cityRef.DeleteAsync();
        }

        public async Task AddToSuspicious(NetworkCellInfo cell)
        {
            await fireStoreDb.Collection("SuspiciousCells").AddAsync(cell);
        }

        public async Task AddToDeleted(NetworkCellInfo cell)
        {
            await fireStoreDb.Collection("DeletedCells").AddAsync(cell);
        }

    }
}
