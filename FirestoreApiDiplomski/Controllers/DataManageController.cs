﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirestoreApiDiplomski.Algorithm;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FirestoreApiDiplomski.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataManageController : ControllerBase
    {
        // GET: api/DataManage
        [HttpGet]
        public async Task<string> Get()
        {
            var dataManager = new DataManage();
            var isSuccessful = await dataManager.CheckAndDeleteUnnecessaryData();

            return isSuccessful ? "Database successfuly cleaned and refreshed" : "Failed";
        }

        // GET: api/DataManage/5
        [HttpGet("{id}")]
        public async Task<string> GetAsync(int id)
        {
            var dataManager = new DataManage();
            var msg = await dataManager.CheckAndDeleteUnnecessaryData();
            return "value";
        }

        // POST: api/DataManage
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/DataManage/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
