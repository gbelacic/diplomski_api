﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirestoreApiDiplomski.Algorithm;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FirestoreApiDiplomski.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultsController : ControllerBase
    {
        ResultsTest _results = new ResultsTest();
        // GET: api/Results/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // GET: api/Results
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    var test = _results.PrepareTestData();
        //    return new string[] { "he he he :)" };
        //}
        // GET: api/UserLocation/2729
        [HttpGet("{data}/{avg}")]
        public async Task<int> GetAsync(int data, int avg)
        {
            if (data >= 0 && data < 3 && avg >= 0 && avg < 2)
            {
                var res = await _results.Test(data, avg);
            }
            return 0;
        }


        [HttpGet]
        public async Task<IEnumerable<string>> GetAsync()
        {
            var prepareData = await _results.Test(0, 0);

            return new string[] { "valsadsue1", "valueasasd2" };
        }



        // POST: api/Results
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Results/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
