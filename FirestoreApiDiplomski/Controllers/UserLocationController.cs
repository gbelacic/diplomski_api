﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Google.Cloud.Firestore;
using FirestoreApiDiplomski.DataAccess;
using FirestoreApiDiplomski.Algorithm;
using FirestoreApiDiplomski.Models;

namespace FirestoreApiDiplomski.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserLocationController : ControllerBase
    {
        PredictUserLocation _algorithm = new PredictUserLocation();

        // GET: api/UserLocation/2729
        [HttpGet("{id}", Name = "Get")]
        public async Task<PredictedLocation> GetAsync(int id)
        {
            var res = await _algorithm.GetLocation(id);
            return res;
        }



        // GET: api/UserLocation
        [HttpGet]
        public async Task<IEnumerable<string>> GetAsync()
        {
            var res = await _algorithm.GetLocation(2729);
            return new string[] { "value1", "value2" };
        }

       

        // POST: api/UserLocation
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/UserLocation/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
